package com.qianc.server.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qianc.entity.RegistryGroupEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RegistryGroupDao extends BaseMapper<RegistryGroupEntity> {



}
