package com.qianc.server.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qianc.model.XxlCacheTemplate;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by xuxueli on 16/8/9.
 */
@Mapper
public interface IXxlCacheTemplateDao extends BaseMapper<XxlCacheTemplate> {

}
