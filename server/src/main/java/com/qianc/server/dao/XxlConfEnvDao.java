package com.qianc.server.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qianc.model.XxlConfEnv;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by xuxueli on 2018-05-30
 */
@Mapper
public interface XxlConfEnvDao extends BaseMapper<XxlConfEnv> {

    public List<XxlConfEnv> findAll();

    public int save(XxlConfEnv xxlConfEnv);

    public int update(XxlConfEnv xxlConfEnv);

    public int delete(@Param("env") String env);

    public XxlConfEnv load(@Param("env") String env);

}