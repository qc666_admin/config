package com.qianc.server.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qianc.entity.RegistryDataEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RegistryDataDao extends BaseMapper<RegistryDataEntity> {


}
