package com.qianc.server.service;

import com.qianc.entity.ReturnT;
import com.qianc.model.XxlCacheTemplate;

import java.util.Map;

/**
 * Created by xuxueli on 16/8/9.
 */
public interface IXxlCacheTemplateService {

    public Map<String,Object> pageList(int offset, int pagesize, String key);

    public ReturnT<String> save(XxlCacheTemplate xxlCacheTemplate);

    public ReturnT<String> update(XxlCacheTemplate xxlCacheTemplate);

    public ReturnT<String> delete(int id);

}
