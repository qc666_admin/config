package com.qianc.server.service;



import com.qianc.entity.RegistryGroupEntity;
import com.qianc.entity.ReturnT;
import com.qianc.core.rpc.registry.impl.xxlrpcadmin.model.XxlRpcAdminRegistryRequest;
import com.qianc.core.rpc.registry.impl.xxlrpcadmin.model.XxlRpcAdminRegistryResponse;

import org.springframework.web.context.request.async.DeferredResult;

import java.util.Map;

/**
 * @author xuxueli 2016-5-28 15:30:33
 */
public interface IXxlRpcRegistryService {

    // admin
    Map<String,Object> pageList(int start, int length, String env, String key);
    ReturnT<String> delete(int id);
    ReturnT<String> update(RegistryGroupEntity xxlRpcRegistry);
    ReturnT<String> add(RegistryGroupEntity xxlRpcRegistry);


    // ------------------------ remote registry ------------------------

    /**
     * refresh registry-value, check update and broacase
     */
    XxlRpcAdminRegistryResponse registry(XxlRpcAdminRegistryRequest registryRequest);

    /**
     * remove registry-value, check update and broacase
     */
    XxlRpcAdminRegistryResponse remove(XxlRpcAdminRegistryRequest registryRequest);

    /**
     * discovery registry-data, read file
     */
    XxlRpcAdminRegistryResponse discovery(XxlRpcAdminRegistryRequest registryRequest);

    /**
     * monitor update
     */
    DeferredResult<XxlRpcAdminRegistryResponse> monitor(XxlRpcAdminRegistryRequest registryRequest);

}
