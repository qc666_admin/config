package com.qianc.server.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qianc.entity.RegistryDataEntity;
import com.qianc.entity.RegistryGroupEntity;
import com.qianc.entity.ReturnT;
import com.qianc.server.common.cache.SystemCache;
import com.qianc.server.common.queue.SystemQueue;
import com.qianc.server.dao.RegistryDataDao;
import com.qianc.server.dao.RegistryGroupDao;
import com.qianc.utils.GsonTool;
import com.qianc.utils.PropUtil;
import com.qianc.core.rpc.registry.impl.xxlrpcadmin.model.XxlRpcAdminRegistryDataItem;
import com.qianc.core.rpc.registry.impl.xxlrpcadmin.model.XxlRpcAdminRegistryRequest;
import com.qianc.core.rpc.registry.impl.xxlrpcadmin.model.XxlRpcAdminRegistryResponse;

import com.qianc.model.XxlRpcRegistryMessage;
import com.qianc.server.dao.IXxlRpcRegistryMessageDao;
import com.qianc.server.service.IXxlRpcRegistryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;

import javax.annotation.Resource;
import java.io.File;
import java.util.*;

/**
 * @author xuxueli 2016-5-28 15:30:33
 */
@Service
public class XxlRpcRegistryServiceImpl implements IXxlRpcRegistryService{
    private static Logger logger = LoggerFactory.getLogger(XxlRpcRegistryServiceImpl.class);

    @Resource
    private RegistryGroupDao registryGroupDao;
    @Resource
    private RegistryDataDao registryDataDao;
    @Resource
    private IXxlRpcRegistryMessageDao xxlRpcRegistryMessageDao;

    @Value("${xxl.rpc.registry.data.filepath}")
    private String registryDataFilePath;
    @Value("${xxl.rpc.registry.accessToken}")
    private String accessToken;




    @Override
    public Map<String, Object> pageList(int start, int length, String env, String key) {

        // page list
        Page<RegistryGroupEntity> page = new Page<>(1,2);
        LambdaQueryWrapper<RegistryGroupEntity> lambda = new QueryWrapper<RegistryGroupEntity>().lambda();
        if(StrUtil.isNotBlank(env)){
            lambda.eq(RegistryGroupEntity::getGroup,env);
        }
        if(StrUtil.isNotBlank(key)){
            lambda.like(RegistryGroupEntity::getAddressList,"%"+key+"%");
        }
        Page<RegistryGroupEntity> page1 = registryGroupDao.selectPage(page, lambda);

        // package result
        Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("data", page1);                      // 分页列表
        return maps;
    }

    @Override
    public ReturnT<String> delete(int id) {
        RegistryGroupEntity xxlRpcRegistry = registryGroupDao.selectById(id);
        if (xxlRpcRegistry != null) {
            registryGroupDao.deleteById(id);

            LambdaQueryWrapper<RegistryDataEntity> lambda = new QueryWrapper<RegistryDataEntity>().lambda();
            lambda.eq(RegistryDataEntity::getEnv,xxlRpcRegistry.getEnv())
                            .eq(RegistryDataEntity::getKey,xxlRpcRegistry.getGroup());
            registryDataDao.delete(lambda);

            // sendRegistryDataUpdateMessage (delete)
            xxlRpcRegistry.setAddress("");
            sendRegistryDataUpdateMessage(xxlRpcRegistry);
        }

        return ReturnT.SUCCESS;
    }



    @Override
    public ReturnT<String> update(RegistryGroupEntity xxlRpcRegistry) {

        // valid
        if (xxlRpcRegistry.getEnv()==null || xxlRpcRegistry.getEnv().trim().length()<2 || xxlRpcRegistry.getEnv().trim().length()>255 ) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, "环境格式非法[2~255]");
        }
        if (xxlRpcRegistry.getGroup()==null || xxlRpcRegistry.getGroup().trim().length()<4 || xxlRpcRegistry.getGroup().trim().length()>255) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, "注册Key格式非法[4~255]");
        }
        if (xxlRpcRegistry.getAddress()==null || xxlRpcRegistry.getAddress().trim().length()==0) {
            xxlRpcRegistry.setAddress(GsonTool.toJson(new ArrayList<String>()));
        }
        List<String> valueList = GsonTool.fromJson(xxlRpcRegistry.getAddress(), List.class);
        if (valueList == null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, "注册Value数据格式非法；限制为字符串数组JSON格式，如 [address,address2]");
        }

        // valid exist
        RegistryGroupEntity exist = registryGroupDao.selectById(xxlRpcRegistry.getId());
        if (exist == null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, "ID参数非法");
        }

        // if refresh
        boolean needMessage = !xxlRpcRegistry.getAddress().equals(exist.getAddress());

        int ret = registryGroupDao.updateById(xxlRpcRegistry);
        needMessage = ret>0?needMessage:false;

        if (needMessage) {
            // sendRegistryDataUpdateMessage (update)
            sendRegistryDataUpdateMessage(xxlRpcRegistry);
        }

        return ret>0?ReturnT.SUCCESS:ReturnT.FAIL;
    }

    @Override
    public ReturnT<String> add(RegistryGroupEntity xxlRpcRegistry) {

        // valid
        if (xxlRpcRegistry.getEnv()==null || xxlRpcRegistry.getEnv().trim().length()<2 || xxlRpcRegistry.getEnv().trim().length()>255 ) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, "环境格式非法[2~255]");
        }
        if (xxlRpcRegistry.getGroup()==null || xxlRpcRegistry.getGroup().trim().length()<4 || xxlRpcRegistry.getGroup().trim().length()>255) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, "注册Key格式非法[4~255]");
        }
        if (xxlRpcRegistry.getAddress()==null || xxlRpcRegistry.getAddress().trim().length()==0) {
            xxlRpcRegistry.setAddress(GsonTool.toJson(new ArrayList<String>()));
        }
        List<String> valueList = GsonTool.fromJson(xxlRpcRegistry.getAddress(), List.class);
        if (valueList == null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, "注册Value数据格式非法；限制为字符串数组JSON格式，如 [address,address2]");
        }

        // valid exist
        LambdaQueryWrapper<RegistryGroupEntity> lambda = new QueryWrapper<RegistryGroupEntity>().lambda();
        lambda.eq(RegistryGroupEntity::getEnv,xxlRpcRegistry.getEnv())
                .eq(RegistryGroupEntity::getGroup,xxlRpcRegistry.getGroup());


        RegistryGroupEntity exist = registryGroupDao.selectOne(lambda);
        if (exist != null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, "注册Key请勿重复");
        }

        int ret = registryGroupDao.insert(xxlRpcRegistry);
        boolean needMessage = ret>0?true:false;

        if (needMessage) {
            // sendRegistryDataUpdateMessage (add)
            sendRegistryDataUpdateMessage(xxlRpcRegistry);
        }

        return ret>0?ReturnT.SUCCESS:ReturnT.FAIL;
    }


    // ------------------------ remote registry ------------------------

    @Override
    public XxlRpcAdminRegistryResponse registry(XxlRpcAdminRegistryRequest registryRequest) {

        // valid
        if (this.accessToken!=null && this.accessToken.trim().length()>0 && !this.accessToken.equals(registryRequest.getAccessToken())) {
            return new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.FAIL_CODE, "AccessToken Invalid");
        }
        if (registryRequest.getEnv()==null || registryRequest.getEnv().trim().length()<2 || registryRequest.getEnv().trim().length()>255) {
            return new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.FAIL_CODE, "Env Invalid[2~255]");
        }
        if (registryRequest.getRegistryDataList()==null || registryRequest.getRegistryDataList().size()==0) {
            return new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.FAIL_CODE, "Registry DataList Invalid");
        }
        for (XxlRpcAdminRegistryDataItem registryData: registryRequest.getRegistryDataList()) {
            if (registryData.getKey()==null || registryData.getKey().trim().length()<4 || registryData.getKey().trim().length()>255) {
                return new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.FAIL_CODE, "Registry Key Invalid[4~255]");
            }
            if (registryData.getValue()==null || registryData.getValue().trim().length()<4 || registryData.getValue().trim().length()>255) {
                return new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.FAIL_CODE, "Registry Value Invalid[4~255]");
            }
        }

        // fill + add queue
        List<RegistryDataEntity> registryDataList = new ArrayList<>();
        for (XxlRpcAdminRegistryDataItem dataItem: registryRequest.getRegistryDataList()) {
            RegistryDataEntity registryData = new RegistryDataEntity();
            registryData.setEnv(registryRequest.getEnv());
            registryData.setKey(dataItem.getKey());
            registryData.setAddress(dataItem.getValue());

            registryDataList.add(registryData);
        }
        SystemQueue.registryQueueAddAll(registryDataList);

        return new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.SUCCESS_CODE, null);
    }

    @Override
    public XxlRpcAdminRegistryResponse remove(XxlRpcAdminRegistryRequest registryRequest) {

        // valid
        if (this.accessToken!=null && this.accessToken.trim().length()>0 && !this.accessToken.equals(registryRequest.getAccessToken())) {
            return new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.FAIL_CODE, "AccessToken Invalid");
        }
        if (registryRequest.getEnv()==null || registryRequest.getEnv().trim().length()<2 || registryRequest.getEnv().trim().length()>255) {
            return new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.FAIL_CODE, "Env Invalid[2~255]");
        }
        if (registryRequest.getRegistryDataList()==null || registryRequest.getRegistryDataList().size()==0) {
            return new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.FAIL_CODE, "Registry DataList Invalid");
        }
        for (XxlRpcAdminRegistryDataItem registryData: registryRequest.getRegistryDataList()) {
            if (registryData.getKey()==null || registryData.getKey().trim().length()<4 || registryData.getKey().trim().length()>255) {
                return new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.FAIL_CODE, "Registry Key Invalid[4~255]");
            }
            if (registryData.getValue()==null || registryData.getValue().trim().length()<4 || registryData.getValue().trim().length()>255) {
                return new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.FAIL_CODE, "Registry Value Invalid[4~255]");
            }
        }

        // fill + add queue
        List<RegistryDataEntity> registryDataList = new ArrayList<>();
        for (XxlRpcAdminRegistryDataItem dataItem: registryRequest.getRegistryDataList()) {
            RegistryDataEntity registryData = new RegistryDataEntity();
            registryData.setEnv(registryRequest.getEnv());
            registryData.setKey(dataItem.getKey());
            registryData.setAddress(dataItem.getValue());

            registryDataList.add(registryData);
        }
        SystemQueue.removeQueueAddAll(registryDataList);

        return new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.SUCCESS_CODE, null);
    }

    @Override
    public XxlRpcAdminRegistryResponse discovery(XxlRpcAdminRegistryRequest registryRequest) {

        // valid
        if (this.accessToken!=null && this.accessToken.trim().length()>0 && !this.accessToken.equals(registryRequest.getAccessToken())) {
            return new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.FAIL_CODE, "AccessToken Invalid");
        }
        if (registryRequest.getEnv()==null || registryRequest.getEnv().trim().length()<2 || registryRequest.getEnv().trim().length()>255) {
            return new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.FAIL_CODE, "Env Invalid[2~255]");
        }
        if (registryRequest.getKeys()==null || registryRequest.getKeys().size()==0) {
            return new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.FAIL_CODE, "keys Invalid.");
        }
        for (String key: registryRequest.getKeys()) {
            if (key==null || key.trim().length()<4 || key.trim().length()>255) {
                return new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.FAIL_CODE, "Key Invalid[4~255]");
            }
        }

        Map<String, TreeSet<String>> result = new HashMap<String, TreeSet<String>>();
        for (String key: registryRequest.getKeys()) {
            // key
            RegistryDataEntity xxlRpcRegistryData = new RegistryDataEntity();
            xxlRpcRegistryData.setEnv(registryRequest.getEnv());
            xxlRpcRegistryData.setKey(key);

            // values
            TreeSet<String> dataList = new TreeSet<String>();
            RegistryGroupEntity fileXxlRpcRegistry = getFileRegistryData(xxlRpcRegistryData);
            if (fileXxlRpcRegistry !=null) {
                dataList.addAll(fileXxlRpcRegistry.getAddressList());
            }

            // fill
            result.put(key, dataList);
        }

        return new XxlRpcAdminRegistryResponse(result);
    }

    @Override
    public DeferredResult<XxlRpcAdminRegistryResponse> monitor(XxlRpcAdminRegistryRequest registryRequest) {

        // init
        DeferredResult deferredResult = new DeferredResult(30 * 1000L, new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.SUCCESS_CODE, "Monitor timeout, no key updated."));

        // valid
        if (this.accessToken!=null && this.accessToken.trim().length()>0 && !this.accessToken.equals(registryRequest.getAccessToken())) {
            deferredResult.setResult(new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.FAIL_CODE, "AccessToken Invalid"));
            return deferredResult;
        }
        if (registryRequest.getEnv()==null || registryRequest.getEnv().trim().length()<2 || registryRequest.getEnv().trim().length()>255) {
            deferredResult.setResult(new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.FAIL_CODE, "Env Invalid[2~255]"));
            return deferredResult;
        }
        if (registryRequest.getKeys()==null || registryRequest.getKeys().size()==0) {
            deferredResult.setResult(new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.FAIL_CODE, "keys Invalid."));
            return deferredResult;
        }
        for (String key: registryRequest.getKeys()) {
            if (key==null || key.trim().length()<4 || key.trim().length()>255) {
                deferredResult.setResult(new XxlRpcAdminRegistryResponse(XxlRpcAdminRegistryResponse.FAIL_CODE, "Key Invalid[4~255]"));
                return deferredResult;
            }
        }

        // monitor by client
        for (String key: registryRequest.getKeys()) {
            String fileName = parseRegistryDataFileName(registryRequest.getEnv(), key);

            List<DeferredResult> deferredResultList = SystemCache.getRegistry(fileName);
            if (deferredResultList == null) {
                deferredResultList = new ArrayList<>();
                SystemCache.putRegistry(fileName, deferredResultList);
            }

            deferredResultList.add(deferredResult);
        }

        return deferredResult;
    }


    // ------------------------ file opt ------------------------

    // get
    public RegistryGroupEntity getFileRegistryData(RegistryDataEntity xxlRpcRegistryData){

        // fileName
        String fileName = parseRegistryDataFileName(xxlRpcRegistryData.getEnv(), xxlRpcRegistryData.getKey());

        // read
        Properties prop = PropUtil.loadProp(fileName);
        if (prop!=null) {
            RegistryGroupEntity fileXxlRpcRegistry = new RegistryGroupEntity();
            fileXxlRpcRegistry.setAddress(prop.getProperty("data"));
            fileXxlRpcRegistry.setStatus(Integer.valueOf(prop.getProperty("status")));
            fileXxlRpcRegistry.setAddressList(GsonTool.fromJson(fileXxlRpcRegistry.getAddress(), List.class));
            return fileXxlRpcRegistry;
        }
        return null;
    }

    public String parseRegistryDataFileName(String env, String key){
        // fileName
        String fileName = registryDataFilePath
                .concat(File.separator).concat(env)
                .concat(File.separator).concat(key)
                .concat(".properties");
        return fileName;
    }

    // set
    public String setFileRegistryData(RegistryGroupEntity xxlRpcRegistry){

        // fileName
        String fileName = parseRegistryDataFileName(xxlRpcRegistry.getEnv(), xxlRpcRegistry.getGroup());

        // valid repeat update
        Properties existProp = PropUtil.loadProp(fileName);
        if (existProp != null
                && existProp.getProperty("data").equals(xxlRpcRegistry.getAddressList())
                && existProp.getProperty("status").equals(String.valueOf(xxlRpcRegistry.getStatus()))
                ) {
            return new File(fileName).getPath();
        }

        // write
        Properties prop = new Properties();
        prop.setProperty("data", GsonTool.toJson(xxlRpcRegistry.getAddressList()));
        prop.setProperty("status", String.valueOf(xxlRpcRegistry.getStatus()));

        PropUtil.writeFileProp(prop, fileName);

        // logger.info(">>>>>>>>>>> xxl-rpc, setFileRegistryData: env={}, key={}, data={}", xxlRpcRegistry.getEnv(), xxlRpcRegistry.getGroup(), xxlRpcRegistry.getAddressList());


        // brocast monitor client
        List<DeferredResult> deferredResultList = SystemCache.getRegistry(fileName);
        if (deferredResultList != null) {
            SystemCache.removeRegistry(fileName);
            for (DeferredResult deferredResult: deferredResultList) {
                deferredResult.setResult(new ReturnT<>(ReturnT.SUCCESS_CODE, "Monitor key update."));
            }
        }

        return new File(fileName).getPath();
    }
    // clean
    public void cleanFileRegistryData(List<String> registryDataFileList){
        filterChildPath(new File(registryDataFilePath), registryDataFileList);
    }

    public void filterChildPath(File parentPath, final List<String> registryDataFileList){
        if (!parentPath.exists() || parentPath.list()==null || parentPath.list().length==0) {
            return;
        }
        File[] childFileList = parentPath.listFiles();
        for (File childFile: childFileList) {
            if (childFile.isFile() && !registryDataFileList.contains(childFile.getPath())) {
                childFile.delete();

                logger.info(">>>>>>>>>>> xxl-rpc, cleanFileRegistryData, RegistryData Path={}", childFile.getPath());
            }
            if (childFile.isDirectory()) {
                if (parentPath.listFiles()!=null && parentPath.listFiles().length>0) {
                    filterChildPath(childFile, registryDataFileList);
                } else {
                    childFile.delete();
                }

            }
        }

    }

    /**
     * update Registry And Message
     */
    public void checkRegistryDataAndSendMessage(RegistryDataEntity xxlRpcRegistryData){
        // data json
        LambdaQueryWrapper<RegistryDataEntity> lambda = new QueryWrapper<RegistryDataEntity>().lambda();
        lambda.eq(RegistryDataEntity::getEnv,xxlRpcRegistryData.getEnv())
                .eq(RegistryDataEntity::getKey,xxlRpcRegistryData.getKey());

        List<RegistryDataEntity> xxlRpcRegistryDataList = registryDataDao.selectList(lambda);
        List<String> valueList = new ArrayList<>();
        if (xxlRpcRegistryDataList !=null && xxlRpcRegistryDataList.size()>0) {
            for (RegistryDataEntity dataItem: xxlRpcRegistryDataList) {
                valueList.add(dataItem.getAddress());
            }
        }
        String dataJson = GsonTool.toJson(valueList);

        // update registry and message
        LambdaQueryWrapper<RegistryGroupEntity> lambda1 = new QueryWrapper<RegistryGroupEntity>().lambda();
        lambda1.eq(RegistryGroupEntity::getEnv,xxlRpcRegistryData.getEnv())
                .eq(RegistryGroupEntity::getGroup,xxlRpcRegistryData.getKey());
        RegistryGroupEntity xxlRpcRegistry = registryGroupDao.selectOne(lambda1);
        boolean needMessage = false;
        if (xxlRpcRegistry == null) {
            xxlRpcRegistry = new RegistryGroupEntity();
            xxlRpcRegistry.setEnv(xxlRpcRegistryData.getEnv());
            xxlRpcRegistry.setGroup(xxlRpcRegistryData.getKey());
            xxlRpcRegistry.setAddressList(valueList);
            registryGroupDao.insert(xxlRpcRegistry);
            needMessage = true;
        } else {

            // check status, locked and disabled not use
            if (xxlRpcRegistry.getStatus() != 0) {
                return;
            }

            if (!GsonTool.toJson(xxlRpcRegistry.getAddressList()).equals(dataJson)) {
                xxlRpcRegistry.setAddressList(valueList);
                registryGroupDao.updateById(xxlRpcRegistry);
                needMessage = true;
            }
        }

        if (needMessage) {
            // sendRegistryDataUpdateMessage (registry update)
            sendRegistryDataUpdateMessage(xxlRpcRegistry);
        }

    }

    /**
     * send RegistryData Update Message
     */
    public void sendRegistryDataUpdateMessage(RegistryGroupEntity xxlRpcRegistry){
        String registryUpdateJson = GsonTool.toJson(xxlRpcRegistry);

        XxlRpcRegistryMessage registryMessage = new XxlRpcRegistryMessage();
        registryMessage.setType(0);
        registryMessage.setData(registryUpdateJson);
        xxlRpcRegistryMessageDao.add(registryMessage);
    }

}
