package com.qianc.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qianc.entity.ReturnT;
import com.qianc.model.XxlCacheTemplate;
import com.qianc.server.dao.IXxlCacheTemplateDao;
import com.qianc.server.service.IXxlCacheTemplateService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by xuxueli on 16/8/9.
 */
@Service
public class XxlCacheTemplateServiceImpl implements IXxlCacheTemplateService {
    private static Logger logger = LogManager.getLogger();


    @Autowired
    private IXxlCacheTemplateDao xxlCacheKeyDao;

    @Override
    public Map<String, Object> pageList(int offset, int pagesize, String key) {

        LambdaQueryWrapper<XxlCacheTemplate> lambda = new QueryWrapper<XxlCacheTemplate>().lambda();
        lambda.eq(XxlCacheTemplate::getKey,key)
                .select(XxlCacheTemplate::getId,XxlCacheTemplate::getKey,XxlCacheTemplate::getIntro);
        IPage<XxlCacheTemplate> page = new Page<>(offset, pagesize);
        IPage<XxlCacheTemplate> xxlCacheTemplateIPage = xxlCacheKeyDao.selectPage(page, lambda);

        // package result
        Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("data", xxlCacheTemplateIPage.getRecords());
        maps.put("recordsTotal", xxlCacheTemplateIPage.getSize());		// 总记录数
        maps.put("recordsFiltered", xxlCacheTemplateIPage.getSize());	// 过滤后的总记录数
        return maps;
    }

    @Override
    public ReturnT<String> save(XxlCacheTemplate xxlCacheTemplate) {
        if (StringUtils.isBlank(xxlCacheTemplate.getKey())) {
            return new ReturnT<String>(500, "请输入“缓存Key”");
        }
        if (StringUtils.isBlank(xxlCacheTemplate.getIntro())) {
            return new ReturnT<String>(500, "请输入“简介”");
        }
        xxlCacheKeyDao.insert(xxlCacheTemplate);
        //xxlCacheKeyDao.save(xxlCacheTemplate);
        return ReturnT.SUCCESS;
    }

    @Override
    public ReturnT<String> update(XxlCacheTemplate xxlCacheTemplate) {
        if (StringUtils.isBlank(xxlCacheTemplate.getKey())) {
            return new ReturnT<String>(500, "请输入“缓存Key”");
        }
        if (StringUtils.isBlank(xxlCacheTemplate.getIntro())) {
            return new ReturnT<String>(500, "请输入“简介”");
        }
        xxlCacheKeyDao.updateById(xxlCacheTemplate);
        //xxlCacheKeyDao.update(xxlCacheTemplate);
        return ReturnT.SUCCESS;
    }

    @Override
    public ReturnT<String> delete(int id) {
        //xxlCacheKeyDao.delete(id);
        xxlCacheKeyDao.deleteById(id);
        return ReturnT.SUCCESS;
    }

}
