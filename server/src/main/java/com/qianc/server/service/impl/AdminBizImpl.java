package com.qianc.server.service.impl;

import com.qianc.param.HandleCallbackParam;
import com.qianc.param.RegistryParam;
import com.qianc.core.job.biz.AdminBiz;
import com.qianc.server.common.job.thread.JobCompleteHelper;
import com.qianc.server.common.job.thread.JobRegistryHelper;
import com.qianc.entity.ReturnT;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author xuxueli 2017-07-27 21:54:20
 */
@Service
public class AdminBizImpl implements AdminBiz {


    @Override
    public ReturnT<String> callback(List<HandleCallbackParam> callbackParamList) {
        return JobCompleteHelper.getInstance().callback(callbackParamList);
    }

    @Override
    public ReturnT<String> registry(RegistryParam registryParam) {
        return JobRegistryHelper.getInstance().registry(registryParam);
    }

    @Override
    public ReturnT<String> registryRemove(RegistryParam registryParam) {
        return JobRegistryHelper.getInstance().registryRemove(registryParam);
    }

}
