package com.qianc.server.common.queue;

import com.qianc.entity.RegistryDataEntity;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class SystemQueue {

    private static volatile LinkedBlockingQueue<RegistryDataEntity> registryQueue = new LinkedBlockingQueue<RegistryDataEntity>();

    private static volatile LinkedBlockingQueue<RegistryDataEntity> removeQueue = new LinkedBlockingQueue<RegistryDataEntity>();

    public static boolean removeQueueAddAll(List<RegistryDataEntity> list){
        return removeQueue.addAll(list);
    }

    public static RegistryDataEntity removeQueueTake(){
        try {
            return removeQueue.take();
        }catch (Exception e){

        }
        return null;
    }

    public static boolean registryQueueAddAll(List<RegistryDataEntity> list){
        return registryQueue.addAll(list);
    }

    public static Long registryQueueCount(){
        return registryQueue.stream().count();
    }

    public static RegistryDataEntity registryQueueTake() throws InterruptedException {

        return registryQueue.take();
    }



}
