package com.qianc.server.common.scheduled;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qianc.entity.RegistryDataEntity;
import com.qianc.entity.RegistryGroupEntity;
import com.qianc.model.XxlRpcRegistryMessage;
import com.qianc.server.common.queue.SystemQueue;
import com.qianc.server.dao.IXxlRpcRegistryMessageDao;
import com.qianc.server.dao.RegistryDataDao;
import com.qianc.server.dao.RegistryGroupDao;
import com.qianc.server.service.impl.XxlRpcRegistryServiceImpl;
import com.qianc.utils.GsonTool;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Slf4j
@Component
public class RegistryScheduledService {

    private static Logger logger = LoggerFactory.getLogger(RegistryScheduledService.class);

    @Resource
    private RegistryGroupDao registryGroupDao;
    @Resource
    private RegistryDataDao registryDataDao;
    @Resource
    private IXxlRpcRegistryMessageDao xxlRpcRegistryMessageDao;

    @Resource
    private XxlRpcRegistryServiceImpl xxlRpcRegistryService;


    private int registryBeatTime = 10;

    private int runCount = 1;

    private volatile boolean executorStoped = false;

    private volatile List<Integer> readedMessageIds = Collections.synchronizedList(new ArrayList<Integer>());

    //@Async
    @Scheduled(fixedDelay = 2 * 1000 )//200ms
    public void registry(){
        for(int i=0;i<runCount;i++){
            try {
                if(SystemQueue.registryQueueCount() == 0){
                    return;
                }
                RegistryDataEntity xxlRpcRegistryData = SystemQueue.registryQueueTake();
                if (xxlRpcRegistryData !=null) {

                    // refresh or add
                    LambdaQueryWrapper<RegistryDataEntity> lambda = new QueryWrapper<RegistryDataEntity>().lambda();
                    lambda.eq(RegistryDataEntity::getEnv,xxlRpcRegistryData.getEnv())
                            .eq(RegistryDataEntity::getKey,xxlRpcRegistryData.getKey())
                            .eq(RegistryDataEntity::getAddress,xxlRpcRegistryData.getAddress());
                    RegistryDataEntity registryDataEntity = new RegistryDataEntity();
                    registryDataEntity.setUpdateTime(new Date());
                    if(StrUtil.isBlankIfStr(xxlRpcRegistryData.getEnv())){
                        System.out.println("error");
                    }
                    int ret = registryDataDao.update(registryDataEntity,lambda);
                    if (ret == 0) {
                        registryDataDao.insert(xxlRpcRegistryData);

                    }

                    // valid file status
                    RegistryGroupEntity fileXxlRpcRegistry = xxlRpcRegistryService.getFileRegistryData(xxlRpcRegistryData);
                    if (fileXxlRpcRegistry == null) {
                        // go on
                    } else if (fileXxlRpcRegistry.getStatus() != 0) {
                        continue;     // "Status limited."
                    } else {
                        if (fileXxlRpcRegistry.getAddressList().contains(xxlRpcRegistryData.getAddress())) {
                            continue;     // "Repeated limited."
                        }
                    }

                    // checkRegistryDataAndSendMessage
                    xxlRpcRegistryService.checkRegistryDataAndSendMessage(xxlRpcRegistryData);
                }
            }catch (Exception e){
                if (!executorStoped) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
    }

    //@Async
    @Scheduled(fixedDelay = 2 * 1000 )//200ms
    public void remove(){
        for(int i=0;i<runCount;i++){
            try {
                RegistryDataEntity xxlRpcRegistryData = SystemQueue.removeQueueTake();
                if (xxlRpcRegistryData != null) {

                    // delete
                    LambdaQueryWrapper<RegistryDataEntity> lambda = new QueryWrapper<RegistryDataEntity>().lambda();
                    lambda.eq(RegistryDataEntity::getEnv,xxlRpcRegistryData.getEnv())
                           .eq(RegistryDataEntity::getKey,xxlRpcRegistryData.getKey())
                           .eq(RegistryDataEntity::getAddress,xxlRpcRegistryData.getAddress());

                    registryDataDao.delete(lambda);

                    //registryDataDao.deleteDataValue(xxlRpcRegistryData.getEnv(), xxlRpcRegistryData.getKey(), xxlRpcRegistryData.getValue());

                    // valid file status
                    RegistryGroupEntity fileXxlRpcRegistry = xxlRpcRegistryService.getFileRegistryData(xxlRpcRegistryData);
                    if (fileXxlRpcRegistry == null) {
                        // go on
                    } else if (fileXxlRpcRegistry.getStatus() != 0) {
                        continue;   // "Status limited."
                    } else {
                        if (!fileXxlRpcRegistry.getAddressList().contains(xxlRpcRegistryData.getAddress())) {
                            continue;   // "Repeated limited."
                        }
                    }

                    // checkRegistryDataAndSendMessage
                    xxlRpcRegistryService.checkRegistryDataAndSendMessage(xxlRpcRegistryData);
                }
            } catch (Exception e) {
                if (!executorStoped) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
    }

    //@Async
    @Scheduled(fixedDelay = 5 * 1000 )//200ms
    public void cleanOldMessage(){
        for(int i=0;i<runCount;i++){
            try {
                // new message, filter readed
                List<XxlRpcRegistryMessage> messageList = xxlRpcRegistryMessageDao.findMessage(readedMessageIds);
                if (messageList!=null && messageList.size()>0) {
                    for (XxlRpcRegistryMessage message: messageList) {
                        readedMessageIds.add(message.getId());

                        if (message.getType() == 0) {   // from registry、add、update、deelete，ne need sync from db, only write

                            RegistryGroupEntity xxlRpcRegistry = GsonTool.fromJson(message.getData(), RegistryGroupEntity.class);

                            // process data by status
                            if (xxlRpcRegistry.getStatus() == 1) {
                                // locked, not updated
                            } else if (xxlRpcRegistry.getStatus() == 2) {
                                // disabled, write empty
                                xxlRpcRegistry.setAddress(GsonTool.toJson(new ArrayList<String>()));
                            } else {
                                // default, sync from db （aready sync before message, only write）
                            }

                            // sync file
                            xxlRpcRegistryService.setFileRegistryData(xxlRpcRegistry);
                        }
                    }
                }

                // clean old message;
                if ( (System.currentTimeMillis()/1000) % registryBeatTime ==0) {
                    xxlRpcRegistryMessageDao.cleanMessage(registryBeatTime);
                    readedMessageIds.clear();
                }
            } catch (Exception e) {
                if (!executorStoped) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
    }

    //@Async
    @Scheduled(fixedDelay = 10000 )//200ms
    public void  cleanOldRegistryDataFile(){
        try {
            // clean old registry-data in db
            LambdaQueryWrapper<RegistryDataEntity> lambda = new QueryWrapper<RegistryDataEntity>().lambda();
            lambda.apply("now() > DATE_ADD(update_time, Interval "+ registryBeatTime * 3 +" SECOND)");
            registryDataDao.delete(lambda);

            //registryDataDao.cleanData(registryBeatTime * 3);

            // sync registry-data, db + file
            int offset = 1;
            int pagesize = 1000;
            List<String> registryDataFileList = new ArrayList<>();

            //List<RegistryGroupEntity> registryList = registryGroupDao.pageList(offset, pagesize, null, null);

            List<RegistryGroupEntity> registryList = registryGroupDao.selectPage(new Page<RegistryGroupEntity>(offset, pagesize), null).getRecords();


            while (registryList!=null && registryList.size()>0) {

                for (RegistryGroupEntity registryItem: registryList) {

                    // process data by status
                    if (registryItem.getStatus() == 1) {
                        // locked, not updated
                    } else if (registryItem.getStatus() == 2) {
                        // disabled, write empty
                        String dataJson = GsonTool.toJson(new ArrayList<String>());
                        registryItem.setAddress(dataJson);
                    } else {
                        // default, sync from db
                        LambdaQueryWrapper<RegistryDataEntity> lambda1 = new QueryWrapper<RegistryDataEntity>().lambda();
                        lambda1.eq(RegistryDataEntity::getEnv,registryItem.getEnv())
                                .eq(RegistryDataEntity::getKey,registryItem.getGroup());
                        List<RegistryDataEntity> xxlRpcRegistryDataList = registryDataDao.selectList(lambda1);
                        //List<RegistryDataEntity> xxlRpcRegistryDataList = registryDataDao.findData(registryItem.getEnv(), registryItem.getKey());
                        List<String> valueList = new ArrayList<String>();
                        if (xxlRpcRegistryDataList !=null && xxlRpcRegistryDataList.size()>0) {
                            for (RegistryDataEntity dataItem: xxlRpcRegistryDataList) {
                                valueList.add(dataItem.getAddress());
                            }
                        }
                        String dataJson = GsonTool.toJson(valueList);

                        // check update, sync db
                        if (!registryItem.getAddress().equals(dataJson)) {
                            registryItem.setAddress(dataJson);
                            registryGroupDao.updateById(registryItem);
                        }
                    }

                    // sync file
                    String registryDataFile = xxlRpcRegistryService.setFileRegistryData(registryItem);

                    // collect registryDataFile
                    registryDataFileList.add(registryDataFile);
                }
                offset += 1000;
                registryList =  registryGroupDao.selectPage(new Page<RegistryGroupEntity>(offset, pagesize), null).getRecords();
                //registryGroupDao.pageList(offset, pagesize, null, null);
            }

            // clean old registry-data file
            xxlRpcRegistryService.cleanFileRegistryData(registryDataFileList);

        } catch (Exception e) {
            if (!executorStoped) {
                logger.error(e.getMessage(), e);
            }
        }
    }





}
