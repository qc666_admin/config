package com.qianc.server.common.job.route.strategy;

import com.qianc.param.TriggerParam;
import com.qianc.server.common.job.route.ExecutorRouter;
import com.qianc.entity.ReturnT;

import java.util.List;

/**
 * Created by xuxueli on 17/3/10.
 */
public class ExecutorRouteLast extends ExecutorRouter {

    @Override
    public ReturnT<String> route(TriggerParam triggerParam, List<String> addressList) {
        return new ReturnT<String>(addressList.get(addressList.size()-1));
    }

}
