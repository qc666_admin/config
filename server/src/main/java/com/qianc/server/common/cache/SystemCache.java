package com.qianc.server.common.cache;

import lombok.Data;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Data
public class SystemCache {

    //注册服务缓存
    private static Map<String, List<DeferredResult>> registryDeferredResultMap = new ConcurrentHashMap<>();

    public static List<DeferredResult> getRegistry(String key){
        return registryDeferredResultMap.get(key);
    }

    public static void putRegistry(String key,List<DeferredResult> value){
        registryDeferredResultMap.put(key,value);
    }

    public static void removeRegistry(String key){
        registryDeferredResultMap.remove(key);
    }




}
