package com.qianc.server.common.job.route.strategy;

import com.qianc.param.TriggerParam;
import com.qianc.server.common.job.route.ExecutorRouter;
import com.qianc.entity.ReturnT;

import java.util.List;
import java.util.Random;

/**
 * Created by xuxueli on 17/3/10.
 */
public class ExecutorRouteRandom extends ExecutorRouter {

    private static Random localRandom = new Random();

    @Override
    public ReturnT<String> route(TriggerParam triggerParam, List<String> addressList) {
        String address = addressList.get(localRandom.nextInt(addressList.size()));
        return new ReturnT<String>(address);
    }

}
