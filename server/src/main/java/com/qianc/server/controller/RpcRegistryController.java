package com.qianc.server.controller;

import com.qianc.entity.RegistryDataEntity;
import com.qianc.entity.RegistryGroupEntity;
import com.qianc.entity.ReturnT;

import com.qianc.server.service.IXxlRpcRegistryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author xuxueli 2018-11-21
 */
@Controller
@RequestMapping("rpc/registry")
public class RpcRegistryController {

    @Resource
    private IXxlRpcRegistryService xxlRpcRegistryService;


    @RequestMapping("")
    public String index(Model model){
        return "registry/registry.index";
    }

    @RequestMapping("/pageList")
    @ResponseBody
    public Map<String, Object> pageList(@RequestParam(required = false, defaultValue = "0") int start,
                                        @RequestParam(required = false, defaultValue = "10") int length,
                                        String env,
                                        String key){
        return xxlRpcRegistryService.pageList(start, length, env, key);
    }

    @RequestMapping("/delete")
    @ResponseBody
    public ReturnT<String> delete(int id){
        return xxlRpcRegistryService.delete(id);
    }

    @RequestMapping("/update")
    @ResponseBody
    public ReturnT<String> update(RegistryGroupEntity xxlRpcRegistry){
        return xxlRpcRegistryService.update(xxlRpcRegistry);
    }

    @RequestMapping("/add")
    @ResponseBody
    public ReturnT<String> add(RegistryGroupEntity xxlRpcRegistry){
        return xxlRpcRegistryService.add(xxlRpcRegistry);
    }




}
