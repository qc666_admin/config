package com.qianc.vo;

import lombok.Data;

import java.util.List;

/**
 * @author xuxueli 2018-12-07
 */
@Data
public class XxlConfParamVO {

    private String accessToken;

    private String env;

    private List<String> keys;

}
