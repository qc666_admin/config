package com.qianc.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.qianc.mybatis.ListJsonTypeHandler;
import lombok.Data;
import org.apache.ibatis.type.JdbcType;

import java.util.Date;
import java.util.List;

@Data
@TableName("t_registry_group")
public class RegistryGroupEntity {

    private Long id;

    private String env;

    @TableField("`group`")
    private String group;

    private Integer status;

    @TableField("`desc`")
    private String desc;

    @TableField("`address_list`")
    private String address;

    //@TableField(jdbcType = JdbcType.VARCHAR, typeHandler = ListJsonTypeHandler.class)
    @TableField(exist = false)
    private List<String> addressList;

    private Date updateTime;
}
