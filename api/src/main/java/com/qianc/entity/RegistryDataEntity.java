package com.qianc.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("t_registry_data")
public class RegistryDataEntity {

    private Long id;

    private String env;

    @TableField("`key`")
    private String key;

    private String address;

    private Date updateTime;

}
