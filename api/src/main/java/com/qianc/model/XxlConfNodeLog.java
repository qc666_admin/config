package com.qianc.model;

import lombok.Data;

import java.util.Date;

/**
 * @author xuxueli 2018-03-01
 */
@Data
public class XxlConfNodeLog {

	private String env;

	private String key;			// 配置Key

	private String title;		// 配置描述

	private String value;		// 配置Value

	private Date addtime;		// 操作时间

	private String optuser;		// 操作人

}
