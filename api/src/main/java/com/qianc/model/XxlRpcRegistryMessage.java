package com.qianc.model;

import lombok.Data;

import java.util.Date;

/**
 * @author xuxueli 2018-11-23
 */
@Data
public class XxlRpcRegistryMessage {

    private int id;

    private int type;         // 消息类型：0-注册更新

    private String data;      // 消息内容

    private Date addTime;

}
