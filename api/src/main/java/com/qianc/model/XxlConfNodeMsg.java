package com.qianc.model;

import lombok.Data;

import java.util.Date;

/**
 * @author xuxueli 2015-9-4 15:26:01
 */
@Data
public class XxlConfNodeMsg {

	private int id;
	private Date addtime;
	private String env;
	private String key;
	private String value;


}
