package com.qianc.model;

import lombok.Data;

/**
 * Created by xuxueli on 2018-05-30
 */
@Data
public class XxlConfEnv {

    private String env;         // Env

    private String title;       // 环境名称

    private int order;

}
