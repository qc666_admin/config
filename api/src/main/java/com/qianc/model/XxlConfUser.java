package com.qianc.model;

import lombok.Data;

/**
 * @author xuxueli 2018-03-01
 */
@Data
public class XxlConfUser {

    private String username;
    private String password;
    private int permission;             // 权限：0-普通用户、1-管理员
    private String permissionData;      // 权限配置数据, 格式 "appname#env,appname#env02"

}
