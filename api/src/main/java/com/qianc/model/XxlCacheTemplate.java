package com.qianc.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * Created by xuxueli on 16/8/9.
 */
@Data
public class XxlCacheTemplate {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String key;

    private String intro;

}
